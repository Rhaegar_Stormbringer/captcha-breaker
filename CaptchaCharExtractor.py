import os
import cv2

'''
Class used to extract all the different letters / numbers from as many captcha as available, in order to construct a database of symbols for the training of the neural network.
'''
class CaptchaCharExtractor():

  def __init__(self, inputDir, outputGeneralDirName="ExtractedChar", outputErrorDirName="_ExtractionError"):
    self.inputDir = inputDir
    self.outputGeneralDirName = outputGeneralDirName
    self.outputErrorDirName = outputErrorDirName
  
  '''
  Extract all the characters of a Captcha, and save each one of them in a separate image.
  file: the captcha file name
  path: the relative path were the captcha is stored. Default is None, and in this case the path provided at the creation of the Extractor (inputDir) will be used in place.
  '''
  def Extract(self, file, path=None, letter_counter = {}):
    if path == None:
      path = self.inputDir
    #letter_counter = {}
    completePath = os.path.join(path, file)
    if not os.path.isfile(completePath):
      print(f'path error: the file "{file}" does not exist in the given directory ({path}).')
      return False
    # Get the text used to save the individual images from the filename, as it contains the captcha text (i.e. "2A2X.png" has the text "2A2X").
    print(f'extracting char from file {file}.')
    text = os.path.splitext(os.path.basename(completePath))[0]
    # loading image
    image = cv2.imread(completePath)
    # used to modification
    image_copy = cv2.imread(completePath)
    image_gray = cv2.imread(completePath, cv2.IMREAD_GRAYSCALE)
    # Simple Thresholding with Otsu's Binarization  - making all pixels pure black & white.
    # We let the function calculate the best threshold value by using the THRESH_OTSU flag, hence the 0 as second parameter.
    # THRESH_BINARY_INV because findcontours() searchs for WHITE object on BLACK background.
    # [1] as we only want the thresholded image (we don't care for the first output).
    image_thres = cv2.threshold(image_gray,0,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    # Getting all letters / numbers of the captcha separated into individual images, in order to build a training database for the neural network.
    contours, hierarchy = cv2.findContours(image_thres, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Drawing found contours on a copy of the original image: used for debbuging as if there is too much contours for the number of letters / numbers in the image, the program return an error and write this modified image instead of the individual ones.
    color_list = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255)]
    for i in range(len(contours)):
      (x, y, w, h) = cv2.boundingRect(contours[i])
      cv2.rectangle(image_copy, (x, y), (x+w, y+h), color_list[i%len(color_list)], 1)

    # Find the regions of each letter / number in the original image
    letter_image_regions = []
    for contour in contours:
      # Get the contour in the original image
      (x, y, w, h) = cv2.boundingRect(contour)
      # Compare the width and height of the contour to detect letters
      # that are conjoined into one chunk.
      if w / h > 1.25:
      # This contour is too wide to be a single letter, split in half.
        half_w = int(w / 2)
        letter_image_regions.append((x, y, half_w, h))
        letter_image_regions.append((x + half_w, y, half_w, h))
      else:
        # This is a normal letter by itself
        letter_image_regions.append((x, y, w, h))

    # If found more or less than 4 regions in the captcha, the letter
    # extraction didn't work correcly -> Skip the image and write the
    # contoured image instead of the individual ones.
    if len(letter_image_regions) != 4:
      print(f'Extraction error on image {text} : found {len(contours)} contours instead of 4.')
      # Grab the saving directory (create it if it doesn't exist").
      error_save_path = os.path.join(".", self.outputErrorDirName)
      if not os.path.exists(error_save_path):
        os.makedirs(error_save_path)
        p = os.path.join(error_save_path, f'{text}.png')
        cv2.imwrite(p, image_copy)
      # Write contoured image
    else:
      # Sort the detected letter images based on the x coordinate.
      letter_image_regions = sorted(letter_image_regions, key=lambda x: x[0])
      # Save out each letter as a single image.
      for letter_bounding_box, letter_text in zip(letter_image_regions, text):
        x, y, w, h = letter_bounding_box
        # Extract the letter from the original image with a 2-pixel margin around the edge.
        letter_image = image[y - 2:y + h + 2, x - 2:x + w + 2]

        # Grab the saving directory (create it if it doesn't exist").
        save_path = os.path.join(os.path.join(".", self.outputGeneralDirName), letter_text)
        
        if not os.path.exists(save_path):
          os.makedirs(save_path)

        # write the letter image to a file
        count = letter_counter.get(letter_text, 1)
        p = os.path.join(save_path, f'{str(count).zfill(6)}.png')
        cv2.imwrite(p, letter_image)
        # increment the count for the current key
        letter_counter[letter_text] = count + 1
  
  '''
  Extract all the characters from all the Captcha contained in the given directory.
  path: the relative path were the captcha is stored. Default is None, and in this case the path provided at the creation of the Extractor (inputDir) will be used in place.
  '''
  def ExtractAll(self, path=None):
      letter_counter = {}
      if path == None:
        path = self.inputDir
      completePath = os.path.join(".", path)
      if not os.path.isdir(completePath):
        print(f'path error: the directory "{completePath}" does not exist.')
        return False
      
      onlyfiles = [f for f in os.listdir(completePath) if os.path.isfile(os.path.join(completePath, f))]

      for file in onlyfiles:
        self.Extract(file=file, path=path, letter_counter=letter_counter)